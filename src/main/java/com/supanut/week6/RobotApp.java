package com.supanut.week6;

public class RobotApp {
    public static void main(String[] args) {
        Robot T_800 = new Robot("T_800", 'T', 0, 0);
        for(int i=0;i<20;i++) {
            T_800.right();
            T_800.down();
        }

        T_800.print();

        Robot R2D2 = new Robot("R2D2", 'R', 1, 1);
        R2D2.print();

        for(int y=Robot.MIN_Y;y<=Robot.MAX_Y; y++) {
            for(int x=Robot.MIN_X; x<=Robot.MAX_X; x++) {
                if(T_800.getX() == x&& T_800.getY() == y) {
                    System.out.print(T_800.getSymbol());
                } else if(R2D2.getX() == x&& R2D2.getY()==y) {
                    System.out.print(R2D2.getSymbol());
                } else {
                    System.out.print("-");
                }
            }
            System.out.println();
        }
    }
    
}
