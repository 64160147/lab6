package com.supanut.week6;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        BookBank supanutYaiMakMak = new BookBank("supanutYaiMakMak",50.0);
        supanutYaiMakMak.print();

        BookBank MonkeyTheMangkud = new BookBank("MonkeyTheMangkud",10000.0);
        MonkeyTheMangkud.print();
        MonkeyTheMangkud.withdraw(4000.0);
        MonkeyTheMangkud.print();

        supanutYaiMakMak.deposit(4000.0);
        supanutYaiMakMak.print();

        BookBank MichaelJacket = new BookBank("MichaelJacket",100000.0);
        MichaelJacket.print();
        MichaelJacket.deposit(2);
        MichaelJacket.print();

    }
}
