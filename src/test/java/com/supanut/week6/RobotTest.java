package com.supanut.week6;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class RobotTest {
    @Test
    public void sholdCreateRobotSuccess1() {
        Robot robot = new Robot("Robot", 'R', 10,11);
        assertEquals("Robot", robot.getName());
        assertEquals('R', robot.getSymbol());
        assertEquals(10, robot.getX());
        assertEquals(11, robot.getY());
    }

    @Test
    public void sholdCreateRobotSuccess2() {
        Robot robot = new Robot("Robot", 'R');
        assertEquals("Robot", robot.getName());
        assertEquals('R', robot.getSymbol());
        assertEquals(0, robot.getX());
        assertEquals(0, robot.getY());
    }

    @Test
    public void sholdDownOver() {
        Robot robot = new Robot("Robot", 'R', 0, Robot.MAX_Y);
        assertEquals(false, robot.down());
        assertEquals(Robot.MAX_Y, robot.getY());
    }

    @Test
    public void sholdUpNegative() {
        Robot robot = new Robot("Robot", 'R', 0, Robot.MIN_Y);
        assertEquals(false, robot.up());
        assertEquals(Robot.MIN_Y, robot.getY());
    }

    @Test
    public void sholdDownSuccess() {
        Robot robot = new Robot("Robot", 'R', 0, 0);
        assertEquals(true, robot.down());
        assertEquals(1, robot.getY());
    }
    @Test
    public void sholdDownNSuccess() {
        Robot robot = new Robot("Robot", 'R', 10,10);
        boolean result = robot.down(5);
        assertEquals(true, result);
        assertEquals(15, robot.getY());
    }
    @Test
    public void sholdDownNFail() {
        Robot robot = new Robot("Robot", 'R', 10, 10);
        boolean result = robot.down(20);
        assertEquals(false, result);
        assertEquals(19, robot.getY());
    }

    @Test
    public void sholdUpSuccess() {
        Robot robot = new Robot("Robot", 'R', 0, 1);
        assertEquals(true, robot.up());
        assertEquals(0, robot.getY());
    }

    @Test
    public void sholdUpNSuccess1() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        assertEquals(true, robot.up(5));
        assertEquals(6, robot.getY());
    }

    @Test
    public void sholdUpNSuccess2() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        boolean result = robot.up(11);
        assertEquals(true, result);
        assertEquals(0, robot.getY());
    }

    @Test
    public void sholdUpNFail1() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        boolean result = robot.up(12);
        assertEquals(false, result);
        assertEquals(0, robot.getY());
    }

    @Test
    public void sholdleftNegative() {

        Robot robot = new Robot("Robot", 'R', Robot.MIN_X,0 );
        assertEquals(false, robot.left());
        assertEquals(Robot.MIN_Y, robot.getX());
    }
    @Test
    public void sholdleftNSuccess() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        boolean result = robot.left(10);
        assertEquals(true, result);
        assertEquals(0, robot.getX());
    }
    @Test
    public void sholdleftNFail() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        boolean result = robot.left(12);
        assertEquals(false, result);
        assertEquals(0, robot.getX());
    }

    @Test
    public void sholdrightNegative() {
        Robot robot = new Robot("Robot", 'R', Robot.MAX_X,0 );
        assertEquals(false, robot.right());
        assertEquals(Robot.MAX_Y, robot.getX());
    }

    @Test
    public void sholdleftSuccess() {
        Robot robot = new Robot("Robot", 'R', 1, 0);
        assertEquals(true, robot.left());
        assertEquals(0, robot.getX());
    }

    @Test
    public void sholdrightSuccess() {
        Robot robot = new Robot("Robot", 'R', 0, 0);
        assertEquals(true, robot.right());
        assertEquals(1, robot.getX());
    }
    @Test
    public void sholdrightNSuccess() {
        Robot robot = new Robot("Robot", 'R', 0, 11);
        boolean result = robot.right(2);
        assertEquals(true, result);
        assertEquals(2, robot.getX());
    }
    @Test
    public void sholdrightNFail() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        boolean result = robot.right(20);
        assertEquals(false, result);
        assertEquals(19, robot.getX());
    }
}
